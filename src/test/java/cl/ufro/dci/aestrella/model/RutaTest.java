package cl.ufro.dci.aestrella.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Pruebas de la clase Ruta")
class RutaTest {

    Ruta ruta;
    DataSet dataSet;
    DataSet dataSetImposible;

    private HashMap<String, String> textos = new HashMap<>();

    @BeforeEach
    void setUp() {
        int anchura = 3;
        int altura = 3;
        dataSet = new DataSet(anchura,altura);
        dataSetImposible = new DataSet(anchura,altura);

        Celda[][] tablero = new Celda[anchura][altura];
        Celda[][] tableroImposible = new Celda[anchura][altura];

        for( int x = 0; x < anchura; x++) {
            for (int y = 0; y < altura; y++) {
                tablero[x][y] = new Celda(x, y);
                tableroImposible[x][y] = new Celda(x, y);
            }
        }
        dataSet.setTablero(tablero);

        tableroImposible[1][1].setObstaculo(true);
        tableroImposible[1][2].setObstaculo(true);
        tableroImposible[2][1].setObstaculo(true);
        dataSetImposible.setTablero(tableroImposible);

        textos.put("msjImposible", "No hay camino posible");
        textos.put("msjCorrecto", "Camino encontrado");
    }

    @Test
    void aStar() {
        //dado un dataSet
        //cuando se calculan la ruta mas corta a un destino posible
        ruta = new Ruta(dataSet);
        //entonces obtener una respuesta afirmativa
        assertEquals(textos.get("msjCorrecto"), ruta.aStar());
    }



    @Test
    void aStarImposible() {
        //dado un dataSet
        //cuando se calculan la ruta mas corta a un destino imposible
        ruta = new Ruta(dataSetImposible);
        //entonces obtener una respuesta negativa
        assertEquals(textos.get("msjImposible"), ruta.aStar());
    }

}