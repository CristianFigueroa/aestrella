package cl.ufro.dci.aestrella.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Pruebas de la clase Celda")
class CeldaTest {

    private DataSet dataSet;

    @BeforeEach
    @DisplayName("Dado un dataSet")
    void setUp() {
        dataSet = new DataSet(100,100);
    }

    @Test
    void calcularVecinos() {
        //dado un tablero
        //cuando se calculan los vecinos de una celda central o que no este en un borde
        List<Celda> vecinos = this.dataSet.getTablero()[1][1].getVecinos();
        //entonces obtener una cantidad de vecinos
        int cantidadEsperada = 4;
        int cantidadRecibida = vecinos.size();
        assertEquals(cantidadEsperada, cantidadRecibida);
    }

    @Test
    void calcularVecinosEsquina() {
        //dado un tablero
        //cuando se calculan los vecinos de las celdas de esquina
        List<Celda> vecinos = this.dataSet.getTablero()[0][0].getVecinos();
        //entonces obtener una cantidad de vecinos
        int cantidadEsperada = 2;
        int cantidadRecibida = vecinos.size();
        assertEquals(cantidadEsperada, cantidadRecibida);
    }

    @Test
    void calcularVecinosConObstaculo() {
        //dado un tablero
        //cuando se calculan los vecinos de las celdas que tienen de vecinos obstaculos
        this.dataSet.getTablero()[1][0].setObstaculo(true);
        this.dataSet.calcularVecinosPorCelda();
        List<Celda> vecinos = this.dataSet.getTablero()[1][1].getVecinos();
        //entonces obtener una cantidad de vecinos
        int cantidadEsperada = 4;
        int cantidadRecibida = vecinos.size();
        assertEquals(cantidadEsperada, cantidadRecibida);
    }
}