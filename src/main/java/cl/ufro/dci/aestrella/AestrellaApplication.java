package cl.ufro.dci.aestrella;

import cl.ufro.dci.aestrella.model.Ruta;
import cl.ufro.dci.aestrella.model.DataSet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AestrellaApplication {

    public static void main(String[] args) {
        SpringApplication.run(AestrellaApplication.class, args);

        DataSet dataSet  = new DataSet(10,10);
        Ruta ruta = new Ruta(dataSet);
        ruta.aStar();
    }
}
