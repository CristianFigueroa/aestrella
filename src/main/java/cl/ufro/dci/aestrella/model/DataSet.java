package cl.ufro.dci.aestrella.model;

public class DataSet {

    private Celda[][] tablero;
    private final int altura;
    private final int anchura;

    public DataSet(int anchura, int altura){
        this.anchura = anchura;
        this.altura = altura;
        inicializarTablero();
        calcularVecinosPorCelda();
    }

    private void inicializarTablero() {
        this.tablero = new Celda[anchura][altura];
        for( int x = 0; x < anchura; x++){
            for (int y=0; y< altura;y++){
                boolean obstaculo = false;
                if(Math.floor(Math.random()*5) == 1){
                    obstaculo = true;
                }
                this.tablero[x][y] = new Celda(x,y);
                this.tablero[x][y].setObstaculo(obstaculo);
            }
        }
    }

    public void calcularVecinosPorCelda() {
        for( int x = 0; x < anchura; x++){
            for (int y=0; y< altura;y++){
                this.tablero[x][y].calcularVecinos(this);
            }
        }
    }

    public Celda getCelda(int x, int y) {
        return tablero[x][y];
    }

    public Celda[][] getTablero() {
        return tablero;
    }

    public void setTablero(Celda[][] tablero) {
        this.tablero = tablero;
        this.calcularVecinosPorCelda();
    }

    public int getAltura() {
        return altura;
    }

    public int getAnchura() {
        return anchura;
    }

    @Override
    public String toString() {
        StringBuilder output = new StringBuilder();
        for( int x = 0; x < anchura; x++){
            for (int y=0; y< altura;y++){
                if(this.tablero[x][y].isObstaculo())
                    output.append(" X ");
                else if(this.tablero[x][y].isCamino())
                    output.append(" O ");
                else
                    output.append(" _ ");
            }
            output.append("\n");
        }
        output.append("-------------------------------------------------------");
        return output.toString();
    }
}
