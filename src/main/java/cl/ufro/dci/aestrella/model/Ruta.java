package cl.ufro.dci.aestrella.model;

import java.util.ArrayList;
import java.util.List;

public class Ruta {

    private DataSet tablero;

    private final Celda inicio;
    private final Celda fin;

    private List<Celda> openSet = new ArrayList<>();
    private List<Celda> closedSet = new ArrayList<>();

    private List<Celda> camino = new ArrayList<>();
    private boolean terminado;

    public Ruta(DataSet tablero) {
        this.tablero = tablero;

        this.inicio = this.tablero.getTablero()[0][0];
        this.tablero.getCelda(0,0).setObstaculo(false);

        this.fin = this.tablero.getTablero()[this.tablero.getAnchura()-1][this.tablero.getAltura()-1];
        this.tablero.getCelda(this.tablero.getAnchura()-1,this.tablero.getAltura()-1).setObstaculo(false);
        this.openSet.add(this.inicio);

        this.terminado = false;

        System.out.println(this.tablero.toString());

    }

    public String aStar(){
        String output =  "";
        while(!this.terminado){
            if((openSet.isEmpty())){
                output = "No hay camino posible";
                break;
            }
            int idCelda = indiceCeldaMenorEsfuerzo(); // Evaluamos cual es la celda que requiere menos esfuerzo para llegar al final
            Celda actual = this.openSet.get(idCelda);
            if(!actual.equals(fin)){
                actualizarValoresCelda(actual);
            }else{
                this.terminado = true;
                Celda temporal = actual;
                camino.add(temporal);
                this.tablero.getCelda(temporal.getX(),temporal.getY()).setCamino(true);

                while(temporal.getPadre()!=null){
                    temporal = temporal.getPadre();
                    camino.add(temporal);
                    this.tablero.getCelda(temporal.getX(),temporal.getY()).setCamino(true);
                }
                System.out.println(this.tablero.toString());
                output = "Camino encontrado";
            }
        }
        System.out.println(output);
        return output;

    }

    private void actualizarValoresCelda(Celda actual){
        openSet.remove(actual);
        closedSet.add(actual);
        for(Celda vecino: actual.getVecinos()) {
            if (!closedSet.contains(vecino) && !vecino.isObstaculo()) {
                int tempG = actual.getG() + 1;

                if (openSet.contains(vecino)) {
                    if (tempG < vecino.getG()) {
                        vecino.setG(tempG);
                    }
                } else {
                    vecino.setG(tempG);
                    openSet.add(vecino);
                }
                vecino.setH(calcularHManhattan(vecino,this.fin));
                vecino.setF(vecino.getG() + vecino.getH());
                vecino.setPadre(actual);
            }
        }
    }

    private int calcularHManhattan(Celda c1, Celda c2){
        int x = Math.abs(c1.getX() - c2.getX());
        int y = Math.abs(c1.getY() - c2.getY());
        return x + y;
    }

    private int indiceCeldaMenorEsfuerzo(){
        int indiceGanador = 0;
        for(int i = 0; i < this.openSet.size(); i++)
            if (this.openSet.get(i).getF() < this.openSet.get(indiceGanador).getF())
                indiceGanador = i;
        return indiceGanador;
    }

}
