package cl.ufro.dci.aestrella.model;

import java.util.ArrayList;
import java.util.List;

public class Celda {

    private Celda padre;
    private List<Celda> vecinos = new ArrayList<>();

    private final int x;
    private final int y;
    boolean obstaculo;
    boolean camino;

    private int f; //coste total (g+h)
    private int g; //pasos dados
    private int h; //estimacion de lo que queda

    public Celda(int x, int y) {
        this.x = x;
        this.y = y;
        this.f = 0;
        this.g = 0;
        this.h = 0;

        this.obstaculo = false;
        this.camino = false;
    }

    public void calcularVecinos(DataSet dataSet) {
        int anchura = dataSet.getAnchura();
        int altura = dataSet.getAnchura();
        Celda[][] tablero = dataSet.getTablero();

        this.vecinos = new ArrayList<>();

        if(y < altura-1) { //abajo
            this.vecinos.add(tablero[x][y + 1]);
        }

        if(x > 0) { //izquierda
            this.vecinos.add(tablero[x - 1][y]);
        }

        if(x < anchura-1) { // derecha
            this.vecinos.add(tablero[x + 1][y]);
        }

        if(y > 0) { // arriba
            this.vecinos.add(tablero[x][y - 1]);
        }

    }

    public List<Celda> getVecinos() {
        return vecinos;
    }

    public Celda getPadre() {
        return padre;
    }

    public void setPadre(Celda padre) {
        this.padre = padre;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isObstaculo() {
        return obstaculo;
    }

    public void setObstaculo(boolean obstaculo) {
        this.obstaculo = obstaculo;
    }

    public int getF() {
        return f;
    }

    public void setF(int f) {
        this.f = f;
    }

    public int getG() {
        return g;
    }

    public void setG(int g) {
        this.g = g;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public boolean isCamino() {
        return camino;
    }

    public void setCamino(boolean camino) {
        this.camino = camino;
    }
}
